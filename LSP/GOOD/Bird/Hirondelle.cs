using System;
using LSP.GOOD.LifeParameters;

namespace LSP.GOOD.Bird
{
    public class Hirondelle : Bird, Powers.Iflying
    {
        public Hirondelle(string name, gender gender) : base(name, gender)
        {
        }

        public int fly_altitude => 12;

        public float fly_celerity => 20;

        public void fly()
        {
            Console.WriteLine("Deploy my wings and fly through this beautiful world");
        }
    }
}