using LSP.GOOD.LifeParameters;

namespace LSP.GOOD.Bird
{
    public abstract class Bird : aliveEntity
    {
        private string name;
        private gender gender;

        public Bird(string name, gender gender)
        {
            this.name = name;
            this.gender = gender;
        }
    }
}