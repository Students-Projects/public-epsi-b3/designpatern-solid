using System;
using LSP.GOOD.LifeParameters;

namespace LSP.GOOD.Bird
{
    public class Autruche : Bird
    {
        public Autruche(string name, gender gender) : base(name, gender)
        {
        }

        public override void eat(String something) => Console.WriteLine("I'm a kind "+this.GetType().Name+" ! Let me eat some "+something+" !");
    }
}