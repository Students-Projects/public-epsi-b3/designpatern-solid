using LSP.GOOD.LifeParameters;

namespace LSP.GOOD.Humainty
{
    public class Woman : Human
    {
        public Woman(string fname, aliveEntity father = null, aliveEntity mother = null) : base(fname, gender.Female, father, mother)
        {
        }
    }
}