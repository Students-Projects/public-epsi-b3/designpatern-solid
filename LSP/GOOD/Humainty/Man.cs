using System;
using LSP.GOOD.LifeParameters;

namespace LSP.GOOD.Humainty
{
    public class Man : Human
    {
        public Man(String fname, aliveEntity father = null, aliveEntity mother = null) : base(fname, gender.Male, father, mother)
        {
        }
    }
}