using System;
using System.Collections;
using LSP.GOOD.LifeParameters;

namespace LSP.GOOD.Humainty
{
    public abstract class Human : aliveEntity,Powers.Iwalk
    {
        private string firstname;
        private string lastname;
        private gender gender;

        public Human(String fname, gender gender, aliveEntity father = null, aliveEntity mother = null) : base(father, mother)
        {
            this.firstname = fname;
            try
            {
                Human papa = (Human) father;
                this.lastname = papa.getLastname();
            } catch(Exception e)
            {
            }

            this.gender = gender;
        }

        public void run(float speed)
        {
            Console.WriteLine("I can run, but i need to be carefull to not fall");
        }

        public void walk(float speed)
        {
            Console.WriteLine("Walking in a park is good for health");
        }

        public String getFirstname() { return this.firstname; }
        public void setLastname(String lastname) { this.firstname = lastname; }
        public String getLastname() { return this.lastname; }
    }
}