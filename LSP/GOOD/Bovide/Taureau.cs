using LSP.GOOD.LifeParameters;

namespace LSP.GOOD.Bovide
{
    public class Taureau : Bovide
    {
        public Taureau(aliveEntity father = null, aliveEntity mother = null) : base(gender.Male, father, mother)
        {
        }
    }
}