using System;
using System.Collections;

namespace LSP.GOOD
{
    public abstract class aliveEntity : Powers.Ieat
    {
        private long lifeTime;


        private aliveEntity father;
        private aliveEntity mother;

        public aliveEntity(aliveEntity father=null, aliveEntity mother=null)
        {
            this.father = father;
            this.mother = mother;
        }

        public virtual void eat(string something)
        {
            Console.WriteLine("Gonna eating "+something+"! gwehehe ");
        }

        public ArrayList getParents()
        {
            ArrayList parents = new ArrayList();

            if(!this.father.Equals(null) || !this.mother.Equals(null))
            {
                if (!this.father.Equals(null))
                    parents.Add(this.father);
                if (!this.mother.Equals(null))
                    parents.Add(this.mother);
            }
            else
            {
                parents.Add("This "+this.GetType().Name+" is orphan");
            }

            return parents;
        }
    }
}