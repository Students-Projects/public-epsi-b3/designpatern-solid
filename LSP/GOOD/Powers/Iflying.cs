namespace LSP.GOOD.Powers
{
    public interface Iflying
    {

        int fly_altitude { get; }
        float fly_celerity { get; }
        void fly();
    }
}