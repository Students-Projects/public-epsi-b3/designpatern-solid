namespace LSP.GOOD.Powers
{
    public interface Iwalk
    {
        void walk(float speed);
        void run(float speed);
    }
}