using LSP.GOOD.Humainty;
using System;

namespace LSP.GOOD
{
    public class Main
    {
        public static void main(string[] args)
        {
            Man A = new Man("Albert");
            A.setLastname("Henri");
            Woman M = new Woman("Michelle");
            M.setLastname("Deva");

            JeanKevin JK = new JeanKevin("Jean", A, M);

            Console.WriteLine(JK.getFirstname, " ", JK.getLastname);
            JK.getMessage();
        }
    }
}